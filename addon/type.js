'use strict';
{
    const PLUGIN_CLASS = SDK.Plugins.Eponesh_GameScore;

    PLUGIN_CLASS.Type = class GameScoreType extends SDK.ITypeBase {
        constructor(sdkPlugin, iObjectType) {
            super(sdkPlugin, iObjectType);
        }
    };
}

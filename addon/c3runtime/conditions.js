'use strict';
{
    function each(runtime, array, cb) {
        const eventSheetManager = runtime.GetEventSheetManager();
        const currentEvent = runtime.GetCurrentEvent();
        const solModifiers = currentEvent.GetSolModifiers();
        const eventStack = runtime.GetEventStack();
        const oldFrame = eventStack.GetCurrentStackFrame();
        const newFrame = eventStack.Push(currentEvent);

        array.forEach((item, index) => {
            cb(item, index);

            eventSheetManager.PushCopySol(solModifiers);
            currentEvent.Retrigger(oldFrame, newFrame);
            eventSheetManager.PopSol(solModifiers);
        });

        eventStack.Pop();
    }

    C3.Plugins.Eponesh_GameScore.Cnds = {
        OnPlayerChange() {
            return true;
        },

        OnPlayerSyncComplete() {
            return true;
        },

        OnPlayerSyncError() {
            return true;
        },

        OnPlayerLoadComplete() {
            return true;
        },

        OnPlayerLoadError() {
            return true;
        },

        OnPlayerLoginComplete() {
            return true;
        },

        OnPlayerLoginError() {
            return true;
        },

        OnPlayerFetchFieldsComplete() {
            return true;
        },

        OnPlayerFetchFieldsError() {
            return true;
        },

        OnPlayerReady() {
            return true;
        },

        IsPlayerReady() {
            return this.isPlayerReady;
        },

        IsPlayerStub() {
            return this.gs.player.isStub;
        },

        IsPlayerLoggedIn() {
            return this.gs.player.isLoggedIn;
        },

        PlayerHasKey(key) {
            return this.gs.player.has(key);
        },

        PlayerFieldIsEnum(key) {
            return this.gs.player.getField(key).variants.length;
        },

        PlayerCompareScore(comparison, value) {
            return this.mappers.compare[comparison](this.gs.player.score, value);
        },

        PlayerCompare(key, comparison, value) {
            return this.mappers.compare[comparison](this.gs.player.get(key), value);
        },

        PlayerEachField() {
            each(this._runtime, this.gs.player.fields, (field) => {
                this.currentPlayerFieldKey = field.key;
                this.currentPlayerFieldType = field.type;
                this.currentPlayerFieldName = field.name;
                this.currentPlayerFieldValue = this.gs.player.get(field.key);
            });

            return false;
        },

        PlayerEachFieldVariant(key) {
            each(this._runtime, this.gs.player.getField(key).variants, (variant, index) => {
                this.currentPlayerFieldVariantValue = variant.value;
                this.currentPlayerFieldVariantName = variant.name;
                this.currentPlayerFieldVariantIndex = index;
            });

            return false;
        },

        OnLeaderboardOpen() {
            return true;
        },

        OnLeaderboardFetch(tag) {
            return this.lastLeaderboardTag === tag;
        },

        OnLeaderboardAnyFetch() {
            return true;
        },

        OnLeaderboardFetchError(tag) {
            return this.lastLeaderboardTag === tag;
        },

        OnLeaderboardAnyFetchError() {
            return true;
        },

        OnLeaderboardFetchPlayer(tag) {
            return this.lastLeaderboardPlayerRatingTag === tag;
        },

        OnLeaderboardAnyFetchPlayer() {
            return true;
        },

        OnLeaderboardFetchPlayerError(tag) {
            return this.lastLeaderboardPlayerRatingTag === tag;
        },

        OnLeaderboardAnyFetchPlayerError() {
            return true;
        },

        OnLeaderboardPublishRecord() {
            return true;
        },

        OnLeaderboardPublishRecordError() {
            return true;
        },

        LeaderboardEachPlayer() {
            each(this._runtime, this.leaderboard, (player, index) => {
                this.currentLeaderboardIndex = index;
                this.currentLeaderboardPlayer = player;
            });

            return false;
        },

        OnAchievementsOpen() {
            return true;
        },

        OnAchievementsClose() {
            return true;
        },

        OnAchievementsFetch() {
            return true;
        },

        OnAchievementsFetchError() {
            return true;
        },

        OnAchievementsUnlock(idOrTag) {
            const id = parseInt(idOrTag, 10) || 0;
            return this.unlockedAchievementTag === idOrTag || this.unlockedAchievementId === id;
        },

        OnAchievementsAnyUnlock() {
            return true;
        },

        OnAchievementsAnyUnlockError() {
            return true;
        },

        AchievementsEachAchievement() {
            each(this._runtime, this.achievements, (achievement, index) => {
                this.currentAchievementIndex = index;
                this.currentAchievementId = achievement.id;
                this.currentAchievementTag = achievement.tag;
                this.currentAchievementName = achievement.name;
                this.currentAchievementDescription = achievement.description;
                this.currentAchievementIcon = achievement.icon;
                this.currentAchievementIconSmall = achievement.iconSmall;
                this.currentAchievementRare = achievement.rare;
                this.currentAchievementUnlocked = this.playerAchievements.some(
                    (a) => a.achievementId === achievement.id
                );
            });

            return false;
        },

        AchievementsEachAchievementInGroup(idOrTag) {
            const id = parseInt(idOrTag, 10) || 0;
            const group = this.achievementsGroups.find((ag) => ag.tag === idOrTag || ag.id === id);
            const achievements = group
                ? group.achievements.reduce((list, aId) => {
                      const achievement = this.achievements.find((a) => a.id === aId);
                      if (achievement) {
                          list.push(achievement);
                      }
                      return list;
                  }, [])
                : [];

            each(this._runtime, achievements, (achievement, index) => {
                this.currentAchievementIndex = index;
                this.currentAchievementId = achievement.id;
                this.currentAchievementTag = achievement.tag;
                this.currentAchievementName = achievement.name;
                this.currentAchievementDescription = achievement.description;
                this.currentAchievementIcon = achievement.icon;
                this.currentAchievementIconSmall = achievement.iconSmall;
                this.currentAchievementRare = achievement.rare;
                this.currentAchievementUnlocked = this.playerAchievements.some(
                    (a) => a.achievementId === achievement.id
                );
            });

            return false;
        },

        AchievementsEachAchievementsGroup() {
            each(this._runtime, this.achievementsGroups, (achievementsGroup, index) => {
                this.currentAchievementsGroupIndex = index;
                this.currentAchievementsGroupId = achievementsGroup.id;
                this.currentAchievementsGroupTag = achievementsGroup.tag;
                this.currentAchievementsGroupName = achievementsGroup.name;
                this.currentAchievementsGroupDescription = achievementsGroup.description;
            });

            return false;
        },

        AchievementsEachPlayerAchievements() {
            each(this._runtime, this.playerAchievements, (playerAchievement, index) => {
                this.currentPlayerAchievementIndex = index;
                this.currentPlayerAchievementId = playerAchievement.achievementId;
                this.currentPlayerAchievementUnlockDate = playerAchievement.createdAt;
            });

            return false;
        },

        IsAchievementsCurAchievementUnlocked() {
            return this.currentAchievementUnlocked;
        },

        IsAchievementsUnlockSuccessful() {
            return this.isUnlockAchievementSuccess;
        },

        AchievementsIsUnlocked(idOrTag) {
            return this.gs.achievements.has(idOrTag);
        },

        // payments
        OnPaymentsFetchProducts() {
            return true;
        },

        OnPaymentsFetchProductsError() {
            return true;
        },

        OnPaymentsPurchase(idOrTag) {
            const id = parseInt(idOrTag, 10) || 0;
            return this.purchasedProductTag === idOrTag || this.purchasedProductId === id;
        },

        OnPaymentsPurchaseError(idOrTag) {
            const id = parseInt(idOrTag, 10) || 0;
            return this.purchasedProductTag === idOrTag || this.purchasedProductId === id;
        },

        OnPaymentsAnyPurchase() {
            return true;
        },

        OnPaymentsAnyPurchaseError() {
            return true;
        },

        OnPaymentsConsume(idOrTag) {
            const id = parseInt(idOrTag, 10) || 0;
            return this.consumedProductTag === idOrTag || this.consumedProductId === id;
        },

        OnPaymentsConsumeError(idOrTag) {
            const id = parseInt(idOrTag, 10) || 0;
            return this.consumedProductTag === idOrTag || this.consumedProductId === id;
        },

        OnPaymentsAnyConsume() {
            return true;
        },

        OnPaymentsAnyConsumeError() {
            return true;
        },

        PaymentsEachProduct() {
            each(this._runtime, this.products, (product, index) => {
                this.currentProductIndex = index;
                this.currentProductId = product.id;
                this.currentProductTag = product.tag;
                this.currentProductName = product.name;
                this.currentProductDescription = product.description;
                this.currentProductIcon = product.icon;
                this.currentProductIconSmall = product.iconSmall;
                this.currentProductPrice = product.price;
                this.currentProductCurrency = product.currency;
                this.currentProductCurrencySymbol = product.currencySymbol;
                this.currentProductPurchases = this.playerPurchases.filter((a) => a.productId === product.id).length;
            });

            return false;
        },

        IsPaymentsCurProductPurchased() {
            return this.currentProductPurchases > 0;
        },

        IsPaymentsPurchaseSuccessful() {
            return this.isPurchaseProductSuccess;
        },

        IsPaymentsConsumeSuccessful() {
            return this.isConsumeProductSuccess;
        },

        PaymentsIsPurchased(idOrTag) {
            return this.gs.payments.has(idOrTag);
        },

        IsPaymentsAvailable() {
            return this.gs.payments.isAvailable;
        },

        OnFullscreenOpen() {
            return true;
        },

        OnFullscreenClose() {
            return true;
        },

        OnFullscreenChange() {
            return true;
        },

        IsFullscreenMode() {
            return this.gs.fullscreen.isEnabled;
        },

        OnAdsStart() {
            return true;
        },

        OnAdsClose() {
            return true;
        },

        OnAdsFullscreenStart() {
            return true;
        },

        OnAdsFullscreenClose() {
            return true;
        },

        OnAdsPreloaderStart() {
            return true;
        },

        OnAdsPreloaderClose() {
            return true;
        },

        OnAdsRewardedStart() {
            return true;
        },

        OnAdsRewardedClose() {
            return true;
        },

        OnAdsRewardedReward() {
            return true;
        },

        OnAdsStickyStart() {
            return true;
        },

        OnAdsStickyClose() {
            return true;
        },

        OnAdsStickyRefresh() {
            return true;
        },

        OnAdsStickyRender() {
            return true;
        },

        IsAdsFullscreenAvailable() {
            return this.gs.ads.isFullscreenAvailable;
        },

        IsAdsRewardedAvailable() {
            return this.gs.ads.isRewardedAvailable;
        },

        IsAdsPreloaderAvailable() {
            return this.gs.ads.isPreloaderAvailable;
        },

        IsAdsStickyAvailable() {
            return this.gs.ads.isStickyAvailable;
        },

        IsAdsFullscreenPlaying() {
            return this.gs.ads.isFullscreenPlaying;
        },

        IsAdsRewardedPlaying() {
            return this.gs.ads.isRewardedPlaying;
        },

        IsAdsPreloaderPlaying() {
            return this.gs.ads.isPreloaderPlaying;
        },

        IsAdsStickyPlaying() {
            return this.gs.ads.isStickyPlaying;
        },

        IsAdsAdblockEnabled() {
            return this.gs.ads.isAdblockEnabled;
        },

        IsAdsLastAdSuccess() {
            return Boolean(this.isLastAdSuccess);
        },

        // gs
        OnChangeLanguage() {
            return true;
        },

        OnChangeAvatarGenerator() {
            return true;
        },

        OnOverlayReady() {
            return true;
        },

        IsDev() {
            return this.gs.isDev;
        },

        Language(language) {
            return this.gs.language === this.mappers.language[language];
        },

        // platform
        HasPlatformIntegratedAuth() {
            return this.gs.platform.hasIntegratedAuth;
        },

        PlatformType(type) {
            return this.gs.platform.type === this.mappers.platform[type];
        },

        IsExternalLinksAllowedOnPlatform() {
            return this.gs.platform.isExternalLinksAllowed;
        },

        // socials
        OnSocialsShare() {
            return true;
        },

        OnSocialsPost() {
            return true;
        },

        OnSocialsInvite() {
            return true;
        },

        OnSocialsJoinCommunity() {
            return true;
        },

        IsSocialsLastShareSuccess() {
            return this.isLastShareSuccess;
        },

        IsSocialsLastCommunityJoinSuccess() {
            return this.isLastCommunityJoinSuccess;
        },

        IsSocialsSupportsNativeShare() {
            return this.gs.socials.isSupportsNativeShare;
        },

        IsSocialsSupportsNativePosts() {
            return this.gs.socials.isSupportsNativePosts;
        },

        IsSocialsSupportsNativeInvite() {
            return this.gs.socials.isSupportsNativeInvite;
        },

        IsSocialsSupportsNativeCommunityJoin() {
            return this.gs.socials.isSupportsNativeCommunityJoin;
        },

        SocialsCanJoinCommunity() {
            return this.gs.socials.canJoinCommunity;
        },

        // games collections
        OnGamesCollectionsOpen() {
            return true;
        },

        OnGamesCollectionsClose() {
            return true;
        },

        OnGamesCollectionsFetchAny() {
            return true;
        },

        OnGamesCollectionsFetchAnyError() {
            return true;
        },

        OnGamesCollectionsFetch(idOrTag) {
            return this.lastGamesCollectionIdOrTag === idOrTag;
        },

        OnGamesCollectionsFetchError(idOrTag) {
            return this.lastGamesCollectionIdOrTag === idOrTag;
        },

        GamesCollectionsEachGame() {
            each(this._runtime, this.gamesCollection.games, (game, index) => {
                this.currentGameIndex = index;
                this.currentGameId = game.id;
                this.currentGameName = game.name;
                this.currentGameDescription = game.description;
                this.currentGameIcon = game.icon;
                this.currentGameUrl = game.url;
            });

            return false;
        },

        IsGamesCollectionsAvailable() {
            return this.gs.gamesCollections.isAvailable;
        },

        // documents
        OnDocumentsOpen() {
            return true;
        },

        OnDocumentsClose() {
            return true;
        },

        OnDocumentsFetchAny() {
            return true;
        },

        OnDocumentsFetchAnyError() {
            return true;
        },

        OnDocumentsFetch(type) {
            return this.lastDocumentType === this.mappers.documentTypes[type];
        },

        OnDocumentsFetchError(type) {
            return this.lastDocumentType === this.mappers.documentTypes[type];
        },

        OnLoadJsonError() {
            return true;
        }
    };
}

'use strict';
{
    C3.Plugins.Eponesh_GameScore.Acts = {
        PlayerSetName(name) {
            this.gs.player.name = name;
        },

        PlayerSetAvatar(src) {
            this.gs.player.avatar = src;
        },

        PlayerSetScore(score) {
            this.gs.player.score = score;
        },

        PlayerAddScore(score) {
            this.gs.player.score += score;
        },

        PlayerSet(key, value) {
            this.gs.player.set(key, value);
        },

        PlayerSetFlag(key, value) {
            this.gs.player.set(key, !value);
        },

        PlayerAdd(key, value) {
            this.gs.player.add(key, value);
        },

        PlayerToggle(key) {
            this.gs.player.toggle(key);
        },

        PlayerReset() {
            this.gs.player.reset();
        },

        PlayerRemove() {
            this.gs.player.remove();
        },

        PlayerSync(override = false) {
            return this.gs.player.sync({ override });
        },

        PlayerLoad() {
            return this.gs.player.load();
        },

        PlayerLogin() {
            return this.gs.player.login();
        },

        PlayerFetchFields() {
            return this.gs.player.fetchFields();
        },

        PlayerWaitForReady() {
            return this.awaiters.player.ready;
        },

        LeaderboardOpen(orderBy, order, limit, withMe, includeFields, displayFields) {
            return this.gs.leaderboard
                .open({
                    id: this.gs.player.id,
                    orderBy: orderBy
                        .split(',')
                        .map((o) => o.trim())
                        .filter((f) => f),
                    order: order === 0 ? 'DESC' : 'ASC',
                    limit,
                    withMe: this.mappers.withMe[withMe],
                    includeFields: includeFields
                        .split(',')
                        .map((o) => o.trim())
                        .filter((f) => f),
                    displayFields: displayFields
                        .split(',')
                        .map((o) => o.trim())
                        .filter((f) => f)
                })
                .catch(console.warn);
        },

        LeaderboardFetch(tag, orderBy, order, limit, withMe, includeFields) {
            return this.gs.leaderboard
                .fetch({
                    id: this.gs.player.id,
                    orderBy: orderBy
                        .split(',')
                        .map((o) => o.trim())
                        .filter((f) => f),
                    order: order === 0 ? 'DESC' : 'ASC',
                    limit,
                    withMe: this.mappers.withMe[withMe],
                    includeFields: includeFields
                        .split(',')
                        .map((o) => o.trim())
                        .filter((f) => f)
                })
                .then((leaderboardInfo) => {
                    this.lastLeaderboardTag = tag;
                    this.lastLeaderboardVariant = 'default';
                    this.leaderboardInfo = leaderboardInfo.leaderboard;
                    this.leaderboard = leaderboardInfo.players;
                    this.Trigger(this.conditions.OnLeaderboardFetch);
                    this.Trigger(this.conditions.OnLeaderboardAnyFetch);
                })
                .catch((err) => {
                    console.warn(err);
                    this.lastLeaderboardTag = tag;
                    this.lastLeaderboardVariant = 'default';
                    this.Trigger(this.conditions.OnLeaderboardFetchError);
                    this.Trigger(this.conditions.OnLeaderboardAnyFetchError);
                });
        },

        LeaderboardFetchPlayerRating(tag, orderBy, order) {
            return this.gs.leaderboard
                .fetchPlayerRating({
                    id: this.gs.player.id,
                    orderBy: orderBy
                        .split(',')
                        .map((o) => o.trim())
                        .filter((f) => f),
                    order: order === 0 ? 'DESC' : 'ASC'
                })
                .then((result) => {
                    this.lastLeaderboardTag = tag;
                    this.lastLeaderboardVariant = 'default';
                    this.lastLeaderboardPlayerRatingTag = tag;
                    this.currentLeaderboardPlayer = Object.assign(this.gs.player.toJSON(), result.player);
                    this.leaderboardPlayerPosition = result.player.position;
                    this.Trigger(this.conditions.OnLeaderboardFetchPlayer);
                    this.Trigger(this.conditions.OnLeaderboardAnyFetchPlayer);
                })
                .catch((err) => {
                    console.warn(err);
                    this.lastLeaderboardTag = tag;
                    this.lastLeaderboardVariant = 'default';
                    this.lastLeaderboardPlayerRatingTag = tag;
                    this.Trigger(this.conditions.OnLeaderboardFetchPlayerError);
                    this.Trigger(this.conditions.OnLeaderboardAnyFetchPlayerError);
                });
        },

        LeaderboardOpenScoped(idOrTag, variant, order, limit, withMe, includeFields, displayFields) {
            const id = parseInt(idOrTag, 10) || 0;
            const query = {
                id,
                tag: idOrTag,
                variant,
                limit,
                order: this.mappers.order[order],
                withMe: this.mappers.withMe[withMe],
                includeFields: includeFields
                    .split(',')
                    .map((o) => o.trim())
                    .filter((f) => f),
                displayFields: displayFields
                    .split(',')
                    .map((o) => o.trim())
                    .filter((f) => f)
            };

            return this.gs.leaderboard.openScoped(query).catch(console.warn);
        },

        LeaderboardFetchScoped(idOrTag, variant, order, limit, withMe, includeFields) {
            const id = parseInt(idOrTag, 10) || 0;
            const query = {
                id,
                tag: idOrTag,
                variant,
                limit,
                order: this.mappers.order[order],
                withMe: this.mappers.withMe[withMe],
                includeFields: includeFields
                    .split(',')
                    .map((o) => o.trim())
                    .filter((f) => f)
            };

            return this.gs.leaderboard
                .fetchScoped(query)
                .then((leaderboardInfo) => {
                    this.lastLeaderboardTag = idOrTag;
                    this.lastLeaderboardVariant = variant;
                    this.leaderboardInfo = leaderboardInfo.leaderboard;
                    this.leaderboard = leaderboardInfo.players;
                    this.Trigger(this.conditions.OnLeaderboardFetch);
                    this.Trigger(this.conditions.OnLeaderboardAnyFetch);
                })
                .catch((err) => {
                    console.warn(err);
                    this.lastLeaderboardTag = idOrTag;
                    this.lastLeaderboardVariant = variant;
                    this.Trigger(this.conditions.OnLeaderboardFetchError);
                    this.Trigger(this.conditions.OnLeaderboardAnyFetchError);
                });
        },

        LeaderboardFetchPlayerRatingScoped(idOrTag, variant, order) {
            const id = parseInt(idOrTag, 10) || 0;
            const query = {
                id,
                tag: idOrTag,
                variant,
                order: this.mappers.order[order]
            };

            return this.gs.leaderboard
                .fetchPlayerRatingScoped(query)
                .then((result) => {
                    this.lastLeaderboardPlayerRatingTag = idOrTag;
                    this.lastLeaderboardTag = idOrTag;
                    this.lastLeaderboardVariant = variant;
                    this.currentLeaderboardPlayer = Object.assign(this.gs.player.toJSON(), result.player);
                    this.leaderboardPlayerPosition = result.player.position;
                    this.Trigger(this.conditions.OnLeaderboardFetchPlayer);
                    this.Trigger(this.conditions.OnLeaderboardAnyFetchPlayer);
                })
                .catch((err) => {
                    console.warn(err);
                    this.lastLeaderboardPlayerRatingTag = idOrTag;
                    this.lastLeaderboardTag = idOrTag;
                    this.lastLeaderboardVariant = variant;
                    this.Trigger(this.conditions.OnLeaderboardFetchPlayerError);
                    this.Trigger(this.conditions.OnLeaderboardAnyFetchPlayerError);
                });
        },

        LeaderboardPublishRecord(idOrTag, variant, override) {
            const recordsTable = this.leaderboardRecords[idOrTag];
            const record = recordsTable ? recordsTable[variant] : null;

            const id = parseInt(idOrTag, 10) || 0;
            const query = {
                id,
                tag: idOrTag,
                variant,
                override,
                record
            };

            return this.gs.leaderboard
                .publishRecord(query)
                .then((result) => {
                    this.lastLeaderboardTag = idOrTag;
                    this.lastLeaderboardVariant = variant;
                    this.lastLeaderboardPlayerRatingTag = idOrTag;
                    this.leaderboardPlayerPosition = result.player.position;
                    this.currentLeaderboardPlayer = Object.assign(this.gs.player.toJSON(), result.player);

                    this.Trigger(this.conditions.OnLeaderboardPublishRecord);
                })
                .catch((err) => {
                    console.warn(err);
                    this.lastLeaderboardTag = idOrTag;
                    this.lastLeaderboardVariant = variant;
                    this.lastLeaderboardPlayerRatingTag = idOrTag;
                    this.Trigger(this.conditions.OnLeaderboardPublishRecordError);
                });
        },

        LeaderboardSetRecord(idOrTag, variant, field, value) {
            if (!this.leaderboardRecords[idOrTag]) {
                this.leaderboardRecords[idOrTag] = {};
            }

            if (!this.leaderboardRecords[idOrTag][variant]) {
                this.leaderboardRecords[idOrTag][variant] = {};
            }

            this.leaderboardRecords[idOrTag][variant][field] = value;
        },

        AchievementsOpen() {
            return this.gs.achievements.open().catch(console.warn);
        },

        AchievementsFetch() {
            return this.gs.achievements
                .fetch()
                .then((result) => {
                    this.achievements = result.achievements;
                    this.achievementsGroups = result.achievementsGroups;
                    this.playerAchievements = result.playerAchievements;
                    this.Trigger(this.conditions.OnAchievementsFetch);
                })
                .catch((err) => {
                    console.warn(err);
                    this.Trigger(this.conditions.OnAchievementsFetchError);
                });
        },

        AchievementsUnlock(idOrTag) {
            const id = parseInt(idOrTag, 10) || 0;
            const query = id > 0 ? { id } : { tag: idOrTag };
            return this.gs.achievements
                .unlock(query)
                .then((result) => {
                    this.isUnlockAchievementSuccess = result.success;
                    this.unlockAchievementError = result.error || '';

                    const achievement = result.achievement || {};
                    this.unlockedAchievementId = achievement.id || 0;
                    this.unlockedAchievementTag = achievement.tag || '';
                    this.unlockedAchievementName = achievement.name || '';
                    this.unlockedAchievementDescription = achievement.description || '';
                    this.unlockedAchievementIcon = achievement.icon || '';
                    this.unlockedAchievementIconSmall = achievement.iconSmall || '';
                    this.unlockedAchievementRare = achievement.rare || 'COMMON';

                    if (result.success) {
                        this.Trigger(this.conditions.OnAchievementsUnlock);
                        this.Trigger(this.conditions.OnAchievementsAnyUnlock);
                        return;
                    }

                    this.Trigger(this.conditions.OnAchievementsAnyUnlockError);
                })
                .catch((err) => {
                    console.warn(err);
                    this.Trigger(this.conditions.OnAchievementsAnyUnlockError);
                });
        },

        PaymentsFetchProducts() {
            return this.gs.payments
                .fetchProducts()
                .then((result) => {
                    this.products = result.products;
                    this.playerPurchases = result.playerPurchases;
                    this.Trigger(this.conditions.OnPaymentsFetchProducts);
                })
                .catch((err) => {
                    console.warn(err);
                    this.Trigger(this.conditions.OnPaymentsFetchProductsError);
                });
        },

        PaymentsPurchase(idOrTag) {
            const id = parseInt(idOrTag, 10) || 0;
            const query = id > 0 ? { id } : { tag: idOrTag };
            return this.gs.payments
                .purchase(query)
                .then((result) => {
                    this.isPurchaseProductSuccess = result.success;
                    this.purchaseProductError = result.error || '';

                    const product = result.product || {};
                    this.purchasedProductId = product.id || 0;
                    this.purchasedProductTag = product.tag || '';

                    if (result.success) {
                        this.Trigger(this.conditions.OnPaymentsPurchase);
                        this.Trigger(this.conditions.OnPaymentsAnyPurchase);
                        return;
                    }

                    this.Trigger(this.conditions.OnPaymentsPurchaseError);
                    this.Trigger(this.conditions.OnPaymentsAnyPurchaseError);
                })
                .catch((err) => {
                    console.warn(err);
                    this.Trigger(this.conditions.OnPaymentsPurchaseError);
                    this.Trigger(this.conditions.OnPaymentsAnyPurchaseError);
                });
        },

        PaymentsConsume(idOrTag) {
            const id = parseInt(idOrTag, 10) || 0;
            const query = id > 0 ? { id } : { tag: idOrTag };
            return this.gs.payments
                .consume(query)
                .then((result) => {
                    this.isConsumeProductSuccess = result.success;
                    this.consumeProductError = result.error || '';

                    const product = result.product || {};
                    this.consumedProductId = product.id || 0;
                    this.consumedProductTag = product.tag || '';

                    if (result.success) {
                        this.Trigger(this.conditions.OnPaymentsConsume);
                        this.Trigger(this.conditions.OnPaymentsAnyConsume);
                        return;
                    }

                    this.Trigger(this.conditions.OnPaymentsConsumeError);
                    this.Trigger(this.conditions.OnPaymentsAnyConsumeError);
                })
                .catch((err) => {
                    console.warn(err);
                    this.Trigger(this.conditions.OnPaymentsConsumeError);
                    this.Trigger(this.conditions.OnPaymentsAnyConsumeError);
                });
        },

        FullscreenOpen() {
            return this.gs.fullscreen.open();
        },

        FullscreenClose() {
            return this.gs.fullscreen.close();
        },

        FullscreenToggle() {
            return this.gs.fullscreen.toggle();
        },

        AdsShowFullscreen() {
            return this.gs.ads.showFullscreen();
        },

        AdsShowRewarded() {
            return this.gs.ads.showRewardedVideo();
        },

        AdsShowPreloader() {
            return this.gs.ads.showPreloader();
        },

        AdsShowSticky() {
            return this.gs.ads.showSticky();
        },

        AdsCloseSticky() {
            return this.gs.ads.closeSticky();
        },

        AdsRefreshSticky() {
            return this.gs.ads.refreshSticky();
        },

        AnalyticsHit(url) {
            return this.gs.analytics.hit(url);
        },

        AnalyticsGoal(event, value) {
            return this.gs.analytics.goal(event, value);
        },

        SocialsShare(text, url, image) {
            return this.gs.socials.share({ text, url, image });
        },

        SocialsPost(text, url, image) {
            return this.gs.socials.post({ text, url, image });
        },

        SocialsInvite(text, url, image) {
            return this.gs.socials.invite({ text, url, image });
        },

        SocialsJoinCommunity() {
            return this.gs.socials.joinCommunity();
        },

        // games collections
        GamesCollectionsOpen(idOrTag) {
            const id = parseInt(idOrTag, 10) || 0;
            const query = id > 0 ? { id } : { tag: idOrTag || 'ANY' };
            return this.gs.gamesCollections.open(query);
        },

        GamesCollectionsFetch(idOrTag) {
            const id = parseInt(idOrTag, 10) || 0;
            const query = id > 0 ? { id } : { tag: idOrTag };
            return this.gs.gamesCollections
                .fetch(query)
                .then((result) => {
                    this.lastGamesCollectionIdOrTag = idOrTag;
                    this.gamesCollection = result;
                    this.Trigger(this.conditions.OnGamesCollectionsFetch);
                    this.Trigger(this.conditions.OnGamesCollectionsFetchAny);
                })
                .catch((err) => {
                    console.warn(err);
                    this.lastGamesCollectionIdOrTag = idOrTag;
                    this.gamesCollectionFetchError = (err && err.message) || '';
                    this.Trigger(this.conditions.OnGamesCollectionsFetchError);
                    this.Trigger(this.conditions.OnGamesCollectionsFetchAnyError);
                });
        },

        // documents
        DocumentsOpen(docType) {
            const type = this.mappers.documentTypes[docType];
            return this.gs.documents.open({ type });
        },

        DocumentsFetch(docType, docFormat) {
            const type = this.mappers.documentTypes[docType];
            const format = this.mappers.documentFormat[docFormat];
            return this.gs.documents
                .fetch({ type, format })
                .then((result) => {
                    this.lastDocumentType = type;
                    this.document = result;
                    this.Trigger(this.conditions.OnDocumentsFetch);
                    this.Trigger(this.conditions.OnDocumentsFetchAny);
                })
                .catch((err) => {
                    console.warn(err);
                    this.lastDocumentType = type;
                    this.documentFetchError = (err && err.message) || '';
                    this.Trigger(this.conditions.OnDocumentsFetchError);
                    this.Trigger(this.conditions.OnDocumentsFetchAnyError);
                });
        },

        ChangeLanguage(language) {
            return this.gs.changeLanguage(this.mappers.language[language]);
        },

        ChangeLanguageByCode(language = '') {
            return this.gs.changeLanguage(language.toLowerCase());
        },

        ChangeAvatarGenerator(generator) {
            return this.gs.changeAvatarGenerator(this.mappers.avatarGenerator[generator]);
        },

        LoadOverlay() {
            return this.gs.loadOverlay();
        },

        LoadFromJSON(data) {
            try {
                const parsed = JSON.parse(data);
                if (!('isReady' in parsed)) {
                    throw new Error('Data was corrupted');
                }

                this.LoadFromJson(parsed);
            } catch (error) {
                this.Trigger(this.conditions.OnLoadJsonError);
            }
        }
    };
}

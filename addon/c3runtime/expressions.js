'use strict';
{
    C3.Plugins.Eponesh_GameScore.Exps = {
        PlayerID() {
            return this.gs.player.id;
        },

        PlayerScore() {
            return this.gs.player.score;
        },

        PlayerName() {
            return this.gs.player.name;
        },

        PlayerAvatar() {
            return this.gs.player.avatar;
        },

        PlayerGet(key) {
            return this.gs.player.get(key);
        },

        PlayerHas(key) {
            return this.gs.player.is(key);
        },

        PlayerFieldName(key) {
            return this.gs.player.getFieldName(key);
        },

        PlayerFieldVariantName(key, value) {
            return this.gs.player.getFieldVariantName(key, value);
        },

        PlayerGetFieldVariantAt(key, index) {
            const variant = this.gs.player.getField(key).variants[index];
            return variant ? variant.value : '';
        },

        PlayerGetFieldVariantIndex(key, value) {
            return this.gs.player.getField(key).variants.findIndex((v) => v.value === value);
        },

        PlayerCurFieldKey() {
            return this.currentPlayerFieldKey || '';
        },

        PlayerCurFieldType() {
            return this.currentPlayerFieldType || '';
        },

        PlayerCurFieldName() {
            return this.currentPlayerFieldName || '';
        },

        PlayerCurFieldValue() {
            return typeof this.currentPlayerFieldValue === 'string'
                ? this.currentPlayerFieldValue
                : Number(this.currentPlayerFieldValue || 0);
        },

        PlayerCurFieldVariantValue() {
            return typeof this.currentPlayerFieldVariantValue === 'string'
                ? this.currentPlayerFieldVariantValue
                : Number(this.currentPlayerFieldVariantValue || 0);
        },

        PlayerCurFieldVariantName() {
            return this.currentPlayerFieldVariantName || '';
        },

        PlayerCurFieldVariantIndex() {
            return this.currentPlayerFieldVariantIndex || 0;
        },

        LeaderboardCurPlayerName() {
            return this.currentLeaderboardPlayer.name || '';
        },

        LeaderboardCurPlayerAvatar() {
            return this.currentLeaderboardPlayer.avatar || '';
        },

        LeaderboardCurPlayerID() {
            return this.currentLeaderboardPlayer.id || 0;
        },

        LeaderboardCurPlayerScore() {
            return this.currentLeaderboardPlayer.score || 0;
        },

        LeaderboardCurPlayerPosition() {
            return this.currentLeaderboardPlayer.position || 0;
        },

        LeaderboardCurPlayerIndex() {
            return this.currentLeaderboardIndex || 0;
        },

        LeaderboardCurPlayerField(key) {
            return key in this.currentLeaderboardPlayer ? this.currentLeaderboardPlayer[key] : 0;
        },

        LeaderboardPlayerFieldAt(index, key) {
            const player = this.leaderboard[index];
            return player && key in player ? player[key] : 0;
        },

        LeaderboardPlayerPosition() {
            return this.leaderboardPlayerPosition || 0;
        },

        LastLeaderboardTag() {
            return this.lastLeaderboardTag;
        },

        LastLeaderboardVariant() {
            return this.lastLeaderboardVariant;
        },

        IsFullscreenMode() {
            return Number(this.gs.fullscreen.isEnabled);
        },

        Language() {
            return this.gs.language;
        },

        AvatarGenerator() {
            return this.gs.avatarGenerator;
        },

        ServerTime() {
            return this.gs.serverTime;
        },

        PlatformType() {
            return this.gs.platform.type;
        },

        AppTitle() {
            return this.gs.app.title;
        },

        AppDescription() {
            return this.gs.app.description;
        },

        AppImage() {
            return this.gs.app.image;
        },

        AppUrl() {
            return this.gs.app.url;
        },

        AchievementsTotalAchievements() {
            return this.achievements.length;
        },

        AchievementsTotalAchievementsGroups() {
            return this.achievementsGroups.length;
        },

        AchievementsTotalPlayerAchievements() {
            return this.playerAchievements.length;
        },

        AchievementsCurAchievementIndex() {
            return this.currentAchievementIndex;
        },

        AchievementsCurAchievementID() {
            return this.currentAchievementId;
        },

        AchievementsCurAchievementTag() {
            return this.currentAchievementTag;
        },

        AchievementsCurAchievementName() {
            return this.currentAchievementName;
        },

        AchievementsCurAchievementDescription() {
            return this.currentAchievementDescription;
        },

        AchievementsCurAchievementIcon() {
            return this.currentAchievementIcon;
        },

        AchievementsCurAchievementIconSmall() {
            return this.currentAchievementIconSmall;
        },

        AchievementsCurAchievementRare() {
            return this.currentAchievementRare;
        },

        AchievementsCurAchievementUnlocked() {
            return this.currentAchievementUnlocked;
        },

        AchievementsCurAchievementsGroupIndex() {
            return this.currentAchievementsGroupIndex;
        },

        AchievementsCurAchievementsGroupID() {
            return this.currentAchievementsGroupID;
        },

        AchievementsCurAchievementsGroupTag() {
            return this.currentAchievementsGroupTag;
        },

        AchievementsCurAchievementsGroupName() {
            return this.currentAchievementsGroupName;
        },

        AchievementsCurAchievementsGroupDescription() {
            return this.currentAchievementsGroupDescription;
        },

        AchievementsCurPlayerAchievementIndex() {
            return this.currentPlayerAchievementIndex;
        },

        AchievementsCurPlayerAchievementID() {
            return this.currentPlayerAchievementId;
        },

        AchievementsCurPlayerAchievementUnlockDate() {
            return this.currentPlayerAchievementUnlockDate;
        },

        AchievementsUnlockedAchievementSuccess() {
            return this.isUnlockAchievementSuccess;
        },

        AchievementsUnlockedAchievementError() {
            return this.unlockAchievementError;
        },

        AchievementsUnlockedAchievementID() {
            return this.unlockedAchievementID;
        },

        AchievementsUnlockedAchievementTag() {
            return this.unlockedAchievementTag;
        },

        AchievementsUnlockedAchievementName() {
            return this.unlockedAchievementName;
        },

        AchievementsUnlockedAchievementDescription() {
            return this.unlockedAchievementDescription;
        },

        AchievementsUnlockedAchievementIcon() {
            return this.unlockedAchievementIcon;
        },

        AchievementsUnlockedAchievementIconSmall() {
            return this.unlockedAchievementIconSmall;
        },

        AchievementsUnlockedAchievementRare() {
            return this.unlockedAchievementRare;
        },

        // socials
        SocialsCommunityLink() {
            return this.gs.socials.communityLink;
        },

        // payments
        PaymentsCurProductIndex() {
            return this.currentProductIndex;
        },

        PaymentsCurProductID() {
            return this.currentProductId;
        },

        PaymentsCurProductTag() {
            return this.currentProductTag;
        },

        PaymentsCurProductName() {
            return this.currentProductName;
        },

        PaymentsCurProductDescription() {
            return this.currentProductDescription;
        },

        PaymentsCurProductIcon() {
            return this.currentProductIcon;
        },

        PaymentsCurProductIconSmall() {
            return this.currentProductIconSmall;
        },

        PaymentsCurProductPrice() {
            return this.currentProductPrice;
        },

        PaymentsCurProductCurrency() {
            return this.currentProductCurrency;
        },

        PaymentsCurProductCurrencySymbol() {
            return this.currentProductCurrencySymbol;
        },

        PaymentsCurProductPurchases() {
            return this.currentProductPurchases;
        },

        PaymentsPurchasedProductSuccess() {
            return this.isPurchaseProductSuccess;
        },

        PaymentsPurchasedProductError() {
            return this.purchaseProductError;
        },

        PaymentsPurchasedProductID() {
            return this.purchasedProductID;
        },

        PaymentsPurchasedProductTag() {
            return this.purchasedProductTag;
        },

        PaymentsConsumedProductSuccess() {
            return this.isConsumeProductSuccess;
        },

        PaymentsConsumedProductError() {
            return this.consumeProductError;
        },

        PaymentsConsumedProductID() {
            return this.consumedProductID;
        },

        PaymentsConsumedProductTag() {
            return this.consumedProductTag;
        },

        // games collections
        GamesCollectionsCollectionID() {
            return this.gamesCollection.id;
        },

        GamesCollectionsCollectionTag() {
            return this.gamesCollection.tag;
        },

        GamesCollectionsCollectionName() {
            return this.gamesCollection.name;
        },

        GamesCollectionsCollectionDescription() {
            return this.gamesCollection.description;
        },

        GamesCollectionsCurGameIndex() {
            return this.currentGameIndex;
        },

        GamesCollectionsCurGameID() {
            return this.currentGameId;
        },

        GamesCollectionsCurGameTag() {
            return this.currentGameTag;
        },

        GamesCollectionsCurGameName() {
            return this.currentGameName;
        },

        GamesCollectionsCurGameDescription() {
            return this.currentGameDescription;
        },

        GamesCollectionsCurGameIcon() {
            return this.currentGameIcon;
        },

        GamesCollectionsCurGameUrl() {
            return this.currentGameUrl;
        },

        GamesCollectionsFetchError() {
            return this.gamesCollectionFetchError;
        },

        // documents
        DocumentsDocumentType() {
            return this.document.type;
        },

        DocumentsDocumentContent() {
            return this.document.content;
        },

        DocumentsFetchError() {
            return this.documentFetchError;
        },

        AsJSON() {
            return JSON.stringify(this.SaveToJson());
        }
    };
}

'use strict';
{
    const SERVER_HOST = 'http://localhost:3000';
    // const SERVER_HOST = 'https://gs.eponesh.com';

    C3.Plugins.Eponesh_GameScore.Instance = class GameScoreInstance extends C3.SDKInstanceBase {
        constructor(inst, properties) {
            super(inst);

            this.mappers = {
                language: ['en', 'ru', 'fr', 'it', 'de', 'es', 'zh', 'pt', 'ko', 'ja'],
                avatarGenerator: [
                    'dicebear_retro',
                    'dicebear_identicon',
                    'dicebear_human',
                    'dicebear_micah',
                    'dicebear_bottts',
                    'icotar',
                    'robohash_robots',
                    'robohash_cats'
                ],
                order: ['default', 'DESC', 'ASC'],
                withMe: ['none', 'first', 'last'],
                platform: ['YANDEX', 'VK', 'NONE', 'OK', 'GAME_MONETIZE', 'CRAZY_GAMES', 'GAME_DISTRIBUTION'],
                documentTypes: ['PLAYER_PRIVACY_POLICY'],
                documentFormat: ['HTML', 'TXT', 'RAW'],
                compare: [
                    (a, b) => a === b,
                    (a, b) => a !== b,
                    (a, b) => a < b,
                    (a, b) => a <= b,
                    (a, b) => a > b,
                    (a, b) => a >= b
                ]
            };

            this.conditions = C3.Plugins.Eponesh_GameScore.Cnds;
            this.actions = C3.Plugins.Eponesh_GameScore.Acts;

            this.awaiters = {
                player: {},
                gs: {}
            };
            this.awaiters.gs.ready = new Promise((res, rej) => {
                this.awaiters.gs.done = res;
                this.awaiters.gs.abort = rej;
            });
            this.awaiters.player.ready = new Promise((res, rej) => {
                this.awaiters.player.done = res;
                this.awaiters.player.abort = rej;
            });

            this.leaderboard = [];
            this.leaderboardInfo = {};
            this.leaderboardRecords = {};
            this.currentLeaderboardIndex = 0;
            this.currentLeaderboardPlayer = {};
            this.lastLeaderboardTag = '';
            this.lastLeaderboardVariant = '';
            this.lastLeaderboardPlayerRatingTag = '';
            this.leaderboardPlayerPosition = 0;

            this.currentPlayerFieldKey = '';
            this.currentPlayerFieldType = '';
            this.currentPlayerFieldName = '';
            this.currentPlayerFieldValue = '';

            this.currentPlayerFieldVariantValue = '';
            this.currentPlayerFieldVariantName = '';
            this.currentPlayerFieldVariantIndex = 0;

            this.achievements = [];
            this.achievementsGroups = [];
            this.playerAchievements = [];

            this.currentAchievementIndex = 0;
            this.currentAchievementId = 0;
            this.currentAchievementTag = '';
            this.currentAchievementName = '';
            this.currentAchievementDescription = '';
            this.currentAchievementIcon = '';
            this.currentAchievementIconSmall = '';
            this.currentAchievementRare = 'COMMON';
            this.currentAchievementUnlocked = false;

            this.currentAchievementsGroupIndex = 0;
            this.currentAchievementsGroupId = 0;
            this.currentAchievementsGroupTag = '';
            this.currentAchievementsGroupName = '';
            this.currentAchievementsGroupDescription = '';

            this.currentPlayerAchievementIndex = 0;
            this.currentPlayerAchievementId = 0;
            this.currentPlayerAchievementUnlockDate = '';

            this.isUnlockAchievementSuccess = false;
            this.unlockAchievementError = '';

            this.unlockedAchievementId = 0;
            this.unlockedAchievementTag = '';
            this.unlockedAchievementName = '';
            this.unlockedAchievementDescription = '';
            this.unlockedAchievementIcon = '';
            this.unlockedAchievementIconSmall = '';
            this.unlockedAchievementRare = 'COMMON';

            this.products = [];
            this.playerPurchases = [];

            this.currentProductIndex = 0;
            this.currentProductId = 0;
            this.currentProductTag = '';
            this.currentProductName = '';
            this.currentProductDescription = '';
            this.currentProductIcon = '';
            this.currentProductIconSmall = '';
            this.currentProductPrice = 0;
            this.currentProductCurrency = '';
            this.currentProductCurrencySymbol = '';
            this.currentProductPurchases = 0;

            this.isPurchaseProductSuccess = false;
            this.purchaseProductError = '';
            this.purchasedProductId = 0;
            this.purchasedProductTag = '';

            this.isConsumeProductSuccess = false;
            this.consumeProductError = '';
            this.consumedProductId = 0;
            this.consumedProductTag = '';

            this.isLastAdSuccess = false;
            this.isLastShareSuccess = false;
            this.isLastCommunityJoinSuccess = false;
            this.isReady = false;
            this.isPlayerReady = false;

            this.gamesCollection = {
                id: 0,
                tag: '',
                name: '',
                description: '',
                games: []
            };

            this.currentGameIndex = 0;
            this.currentGameId = 0;
            this.currentGameName = '';
            this.currentGameDescription = '';
            this.currentGameIcon = '';
            this.currentGameUrl = '';
            this.gamesCollectionFetchError = '';
            this.lastGamesCollectionIdOrTag = '';

            this.document = {
                type: '',
                content: ''
            };

            this.lastDocumentType = '';
            this.documentFetchError = '';

            this.projectId = Number(properties[0] || 0);
            this.publicToken = properties[1];
            this.showPreloaderOnStart = properties[2];
            this.shouldWaitPlayerOnLoad = properties[3];

            this._runtime.AddLoadPromise(this.awaiters.gs.ready);
            if (this.shouldWaitPlayerOnLoad) {
                this._runtime.AddLoadPromise(this.awaiters.player.ready);
            }

            this._runtime.Dispatcher().addEventListener('afterfirstlayoutstart', () => {
                if (this.isReady) {
                    this.Trigger(this.conditions.OnReady);
                }

                if (this.isPlayerReady) {
                    this.Trigger(this.conditions.OnPlayerReady);
                }
            });

            this.loadLib();
        }

        onError() {
            const stub = () => Promise.resolve({});
            this.awaiters.gs.done();
            this.awaiters.player.done();
            this.gs = {
                on() {},
                changeLanguage: stub,
                changeAvatarGenerator: stub,
                loadOverlay: stub,
                isDev: false,
                language: 'en',
                avatarGenerator: 'dicebear_retro',
                app: {
                    title: '',
                    description: '',
                    image: '',
                    url: ''
                },
                analytics: {
                    on() {},
                    hit() {},
                    goal() {}
                },
                platform: {
                    on() {},
                    hasIntegratedAuth: false,
                    type: 'NONE'
                },
                socials: {
                    isSupportsNativeShare: false,
                    isSupportsNativePosts: false,
                    isSupportsNativeInvite: false,
                    share: stub,
                    post: stub,
                    invite: stub
                },
                leaderboard: {
                    on() {},
                    open: stub,
                    fetch: stub,
                    fetchPlayerRating: stub
                },
                achievements: {
                    on() {},
                    has() {},
                    open: stub,
                    fetch: stub,
                    unlock: stub
                },
                gamesCollections: {
                    on() {},
                    open: stub,
                    fetch: stub
                },
                documents: {
                    on() {},
                    open: stub,
                    fetch: stub
                },
                payments: {
                    isAvailable: false,
                    on() {},
                    has() {},
                    fetchProducts: stub,
                    purchase: stub,
                    consume: stub
                },
                fullscreen: {
                    isEnabled: false,
                    on() {},
                    open() {},
                    close() {},
                    toggle() {}
                },
                ads: {
                    isFullscreenAvailable: false,
                    isRewardedAvailable: false,
                    isPreloaderAvailable: false,
                    isStickyAvailable: false,
                    isAdblockEnabled: false,
                    on() {},
                    showFullscreen: stub,
                    showRewardedVideo: stub,
                    showPreloader: stub,
                    showSticky: stub,
                    closeSticky: stub,
                    refreshSticky: stub
                },
                player: {
                    isStub: true,
                    isLoggedIn: false,
                    id: 0,
                    name: '',
                    avatar: '',
                    on() {},
                    sync: stub,
                    load: stub,
                    login: stub,
                    fetchFields: stub,
                    getField: stub,
                    getFieldName: stub,
                    getFieldVariantName: stub,
                    add: stub,
                    has: stub,
                    get: stub,
                    set: stub,
                    toggle: stub,
                    reset: stub,
                    remove: stub,
                    fields: []
                }
            };
            this.isReady = true;
            this.Trigger(this.conditions.OnReady);
            this.isPlayerReady = true;
            this.Trigger(this.conditions.OnPlayerReady);
        }

        loadLib() {
            try {
                window.onGSInit = (gs) => gs.ready.then(() => this.init(gs)).catch(() => this.onError());
                ((d) => {
                    var t = d.getElementsByTagName('script')[0];
                    var s = d.createElement('script');
                    s.src = `${SERVER_HOST}/sdk/game-score.js?projectId=${this.projectId}&publicToken=${this.publicToken}&callback=onGSInit`;
                    s.async = true;
                    s.onerror = () => this.onError();
                    t.parentNode.insertBefore(s, t);
                })(document);
            } catch (err) {
                console.error(err);
                this.onError();
            }
        }

        init(gs) {
            this.gs = gs;
            this._runtime.GetIRuntime().GameScore = gs;

            // player
            this.gs.player.on('ready', () => {
                this.isPlayerReady = true;
                this.awaiters.player.done();
                this.Trigger(this.conditions.OnPlayerReady);
            });
            this.gs.player.on('change', () => this.Trigger(this.conditions.OnPlayerChange));
            this.gs.player.on('sync', (success) => {
                this.Trigger(success ? this.conditions.OnPlayerSyncComplete : this.conditions.OnPlayerSyncError);
            });
            this.gs.player.on('load', (success) => {
                this.Trigger(success ? this.conditions.OnPlayerLoadComplete : this.conditions.OnPlayerLoadError);
            });
            this.gs.player.on('login', (success) => {
                this.Trigger(success ? this.conditions.OnPlayerLoginComplete : this.conditions.OnPlayerLoginError);
            });
            this.gs.player.on('fetchFields', (success) => {
                this.Trigger(
                    success ? this.conditions.OnPlayerFetchFieldsComplete : this.conditions.OnPlayerFetchFieldsError
                );
            });

            // leaderboard
            this.gs.leaderboard.on('open', () => this.Trigger(this.conditions.OnLeaderboardOpen));

            // achievements
            this.gs.achievements.on('open', () => this.Trigger(this.conditions.OnAchievementsOpen));
            this.gs.achievements.on('close', () => this.Trigger(this.conditions.OnAchievementsClose));

            // games collections
            this.gs.gamesCollections.on('open', () => this.Trigger(this.conditions.OnGamesCollectionsOpen));
            this.gs.gamesCollections.on('close', () => this.Trigger(this.conditions.OnGamesCollectionsClose));

            this.gs.documents.on('open', () => this.Trigger(this.conditions.OnDocumentsOpen));
            this.gs.documents.on('close', () => this.Trigger(this.conditions.OnDocumentsClose));

            // fullscreen
            this.gs.fullscreen.on('open', () => this.Trigger(this.conditions.OnFullscreenOpen));
            this.gs.fullscreen.on('close', () => this.Trigger(this.conditions.OnFullscreenClose));
            this.gs.fullscreen.on('change', () => this.Trigger(this.conditions.OnFullscreenChange));

            // ads
            this.gs.ads.on('start', () => this.Trigger(this.conditions.OnAdsStart));
            this.gs.ads.on('close', (success) => {
                this.isLastAdSuccess = success;
                this.Trigger(this.conditions.OnAdsClose);
            });

            this.gs.ads.on('fullscreen:start', () => this.Trigger(this.conditions.OnAdsFullscreenStart));
            this.gs.ads.on('fullscreen:close', () => this.Trigger(this.conditions.OnAdsFullscreenClose));

            this.gs.ads.on('preloader:start', () => this.Trigger(this.conditions.OnAdsPreloaderStart));
            this.gs.ads.on('preloader:close', () => this.Trigger(this.conditions.OnAdsPreloaderClose));

            this.gs.ads.on('rewarded:start', () => this.Trigger(this.conditions.OnAdsRewardedStart));
            this.gs.ads.on('rewarded:close', () => this.Trigger(this.conditions.OnAdsRewardedClose));
            this.gs.ads.on('rewarded:reward', () => this.Trigger(this.conditions.OnAdsRewardedReward));

            this.gs.ads.on('sticky:start', () => this.Trigger(this.conditions.OnAdsStickyStart));
            this.gs.ads.on('sticky:close', () => this.Trigger(this.conditions.OnAdsStickyClose));
            this.gs.ads.on('sticky:refresh', () => this.Trigger(this.conditions.OnAdsStickyRefresh));
            this.gs.ads.on('sticky:render', () => this.Trigger(this.conditions.OnAdsStickyRender));

            // socials
            this.gs.socials.on('share', (success) => {
                this.isLastShareSuccess = success;
                this.Trigger(this.conditions.OnSocialsShare);
            });
            this.gs.socials.on('post', (success) => {
                this.isLastShareSuccess = success;
                this.Trigger(this.conditions.OnSocialsPost);
            });
            this.gs.socials.on('invite', (success) => {
                this.isLastShareSuccess = success;
                this.Trigger(this.conditions.OnSocialsInvite);
            });
            this.gs.socials.on('joinCommunity', (success) => {
                this.isLastCommunityJoinSuccess = success;
                this.Trigger(this.conditions.OnSocialsJoinCommunity);
            });

            // gs
            this.gs.on('change:language', () => this.Trigger(this.conditions.OnChangeLanguage));
            this.gs.on('change:avatarGenerator', () => this.Trigger(this.conditions.OnChangeAvatarGenerator));
            this.gs.on('overlay:ready', () => this.Trigger(this.conditions.OnOverlayReady));

            // ready
            this.isReady = true;
            this.Trigger(this.conditions.OnReady);
            this.awaiters.gs.done();

            if (this.showPreloaderOnStart) {
                this.gs.ads.showPreloader();
            }
        }

        Release() {
            super.Release();
        }

        SaveToJson() {
            return {
                leaderboard: this.leaderboard,
                leaderboardInfo: this.leaderboardInfo,
                leaderboardRecords: this.leaderboardRecords,

                currentLeaderboardIndex: this.currentLeaderboardIndex,
                currentLeaderboardPlayer: this.currentLeaderboardPlayer,
                lastLeaderboardTag: this.lastLeaderboardTag,
                lastLeaderboardVariant: this.lastLeaderboardVariant,
                lastLeaderboardPlayerRatingTag: this.lastLeaderboardPlayerRatingTag,
                leaderboardPlayerPosition: this.leaderboardPlayerPosition,

                currentPlayerFieldKey: this.currentPlayerFieldKey,
                currentPlayerFieldType: this.currentPlayerFieldType,
                currentPlayerFieldName: this.currentPlayerFieldName,
                currentPlayerFieldValue: this.currentPlayerFieldValue,

                currentPlayerFieldVariantValue: this.currentPlayerFieldVariantValue,
                currentPlayerFieldVariantName: this.currentPlayerFieldVariantName,
                currentPlayerFieldVariantIndex: this.currentPlayerFieldVariantIndex,

                isLastAdSuccess: this.isLastAdSuccess,
                isLastShareSuccess: this.isLastShareSuccess,
                isLastCommunityJoinSuccess: this.isLastCommunityJoinSuccess,
                isReady: this.isReady,
                isPlayerReady: this.isPlayerReady,

                achievements: this.achievements,
                achievementsGroups: this.achievementsGroups,
                playerAchievements: this.playerAchievements,

                currentAchievementIndex: this.currentAchievementIndex,
                currentAchievementId: this.currentAchievementId,
                currentAchievementTag: this.currentAchievementTag,
                currentAchievementName: this.currentAchievementName,
                currentAchievementDescription: this.currentAchievementDescription,
                currentAchievementIcon: this.currentAchievementIcon,
                currentAchievementIconSmall: this.currentAchievementIconSmall,
                currentAchievementRare: this.currentAchievementRare,
                currentAchievementUnlocked: this.currentAchievementUnlocked,

                currentAchievementsGroupIndex: this.currentAchievementsGroupIndex,
                currentAchievementsGroupId: this.currentAchievementsGroupId,
                currentAchievementsGroupTag: this.currentAchievementsGroupTag,
                currentAchievementsGroupName: this.currentAchievementsGroupName,
                currentAchievementsGroupDescription: this.currentAchievementsGroupDescription,

                currentPlayerAchievementIndex: this.currentPlayerAchievementIndex,
                currentPlayerAchievementId: this.currentPlayerAchievementId,
                currentPlayerAchievementUnlockDate: this.currentPlayerAchievementUnlockDate,

                isUnlockAchievementSuccess: this.isUnlockAchievementSuccess,
                unlockAchievementError: this.unlockAchievementError,

                unlockedAchievementId: this.unlockedAchievementId,
                unlockedAchievementTag: this.unlockedAchievementTag,
                unlockedAchievementName: this.unlockedAchievementName,
                unlockedAchievementDescription: this.unlockedAchievementDescription,
                unlockedAchievementIcon: this.unlockedAchievementIcon,
                unlockedAchievementIconSmall: this.unlockedAchievementIconSmall,
                unlockedAchievementRare: this.unlockedAchievementRare,

                products: this.products,
                playerPurchases: this.playerPurchases,

                currentProductIndex: this.currentProductIndex,
                currentProductId: this.currentProductId,
                currentProductTag: this.currentProductTag,
                currentProductName: this.currentProductName,
                currentProductDescription: this.currentProductDescription,
                currentProductIcon: this.currentProductIcon,
                currentProductIconSmall: this.currentProductIconSmall,
                currentProductPrice: this.currentProductPrice,
                currentProductCurrency: this.currentProductCurrency,
                currentProductCurrencySymbol: this.currentProductCurrencySymbol,
                currentProductPurchases: this.currentProductPurchases,

                isPurchaseProductSuccess: this.isPurchaseProductSuccess,
                purchaseProductError: this.purchaseProductError,
                purchasedProductId: this.purchasedProductId,
                purchasedProductTag: this.purchasedProductTag,

                isConsumeProductSuccess: this.isConsumeProductSuccess,
                consumeProductError: this.consumeProductError,
                consumedProductId: this.consumedProductId,
                consumedProductTag: this.consumedProductTag,

                gamesCollection: this.gamesCollection,

                currentGameIndex: this.currentGameIndex,
                currentGameId: this.currentGameId,
                currentGameName: this.currentGameName,
                currentGameDescription: this.currentGameDescription,
                currentGameIcon: this.currentGameIcon,
                currentGameUrl: this.currentGameUrl,

                gamesCollectionFetchError: this.gamesCollectionFetchError,
                lastGamesCollectionIdOrTag: this.lastGamesCollectionIdOrTag,

                document: this.document,
                lastDocumentType: this.lastDocumentType,
                documentFetchError: this.documentFetchError
            };
        }

        LoadFromJson(o) {
            this.leaderboard = o.leaderboard;
            this.leaderboardInfo = o.leaderboardInfo || {};
            this.leaderboardRecords = o.leaderboardRecords || {};

            this.currentLeaderboardIndex = o.currentLeaderboardIndex;
            this.currentLeaderboardPlayer = o.currentLeaderboardPlayer;
            this.lastLeaderboardTag = o.lastLeaderboardTag;
            this.lastLeaderboardVariant = o.lastLeaderboardVariant;
            this.lastLeaderboardPlayerRatingTag = o.lastLeaderboardPlayerRatingTag;
            this.leaderboardPlayerPosition = o.leaderboardPlayerPosition || 0;

            this.currentPlayerFieldKey = o.currentPlayerFieldKey;
            this.currentPlayerFieldType = o.currentPlayerFieldType;
            this.currentPlayerFieldName = o.currentPlayerFieldName;
            this.currentPlayerFieldValue = o.currentPlayerFieldValue;

            this.currentPlayerFieldVariantValue = o.currentPlayerFieldVariantValue;
            this.currentPlayerFieldVariantName = o.currentPlayerFieldVariantName;
            this.currentPlayerFieldVariantIndex = o.currentPlayerFieldVariantIndex;

            this.isLastAdSuccess = o.isLastAdSuccess;
            this.isLastShareSuccess = o.isLastShareSuccess;
            this.isLastCommunityJoinSuccess = o.isLastCommunityJoinSuccess;
            this.isReady = o.isReady;
            this.isPlayerReady = o.isPlayerReady;

            this.achievements = o.achievements || [];
            this.achievementsGroups = o.achievementsGroups || [];
            this.playerAchievements = o.playerAchievements || [];

            this.currentAchievementIndex = o.currentAchievementIndex || 0;
            this.currentAchievementId = o.currentAchievementId || 0;
            this.currentAchievementTag = o.currentAchievementTag || '';
            this.currentAchievementName = o.currentAchievementName || '';
            this.currentAchievementDescription = o.currentAchievementDescription || '';
            this.currentAchievementIcon = o.currentAchievementIcon || '';
            this.currentAchievementIconSmall = o.currentAchievementIconSmall || '';
            this.currentAchievementRare = o.currentAchievementRare || 'COMMON';
            this.currentAchievementUnlocked = o.currentAchievementUnlocked || false;

            this.currentAchievementsGroupIndex = o.currentAchievementsGroupIndex || 0;
            this.currentAchievementsGroupId = o.currentAchievementsGroupId || 0;
            this.currentAchievementsGroupTag = o.currentAchievementsGroupTag || '';
            this.currentAchievementsGroupName = o.currentAchievementsGroupName || '';
            this.currentAchievementsGroupDescription = o.currentAchievementsGroupDescription || '';

            this.currentPlayerAchievementIndex = o.currentPlayerAchievementIndex || 0;
            this.currentPlayerAchievementId = o.currentPlayerAchievementId || 0;
            this.currentPlayerAchievementUnlockDate = o.currentPlayerAchievementUnlockDate || '';

            this.isUnlockAchievementSuccess = o.isUnlockAchievementSuccess || false;
            this.unlockAchievementError = o.unlockAchievementError || '';

            this.unlockedAchievementId = o.unlockedAchievementId || 0;
            this.unlockedAchievementTag = o.unlockedAchievementTag || '';
            this.unlockedAchievementName = o.unlockedAchievementName || '';
            this.unlockedAchievementDescription = o.unlockedAchievementDescription || '';
            this.unlockedAchievementIcon = o.unlockedAchievementIcon || '';
            this.unlockedAchievementIconSmall = o.unlockedAchievementIconSmall || '';
            this.unlockedAchievementRare = o.unlockedAchievementRare || 'COMMON';

            this.products = o.products || [];
            this.playerPurchases = o.playerPurchases || [];

            this.currentProductIndex = o.currentProductIndex || 0;
            this.currentProductId = o.currentProductId || 0;
            this.currentProductTag = o.currentProductTag || '';
            this.currentProductName = o.currentProductName || '';
            this.currentProductDescription = o.currentProductDescription || '';
            this.currentProductIcon = o.currentProductIcon || '';
            this.currentProductIconSmall = o.currentProductIconSmall || '';
            this.currentProductPrice = o.currentProductPrice || 0;
            this.currentProductCurrency = o.currentProductCurrency || '';
            this.currentProductCurrencySymbol = o.currentProductCurrencySymbol || '';
            this.currentProductPurchases = o.currentProductPurchases || 0;

            this.isPurchaseProductSuccess = o.isPurchaseProductSuccess || false;
            this.purchaseProductError = o.purchaseProductError || '';
            this.purchasedProductId = o.purchasedProductId || 0;
            this.purchasedProductTag = o.purchasedProductTag || '';

            this.isConsumeProductSuccess = o.isConsumeProductSuccess || false;
            this.consumeProductError = o.consumeProductError || '';
            this.consumedProductId = o.consumedProductId || 0;
            this.consumedProductTag = o.consumedProductTag || '';

            this.gamesCollection = o.gamesCollection || {
                id: 0,
                tag: '',
                name: '',
                description: '',
                games: []
            };

            this.currentGameIndex = o.currentGameIndex || 0;
            this.currentGameId = o.currentGameId || 0;
            this.currentGameName = o.currentGameName || '';
            this.currentGameDescription = o.currentGameDescription || '';
            this.currentGameIcon = o.currentGameIcon || '';
            this.currentGameUrl = o.currentGameUrl || '';

            this.gamesCollectionFetchError = o.gamesCollectionFetchError || '';
            this.lastGamesCollectionIdOrTag = o.lastGamesCollectionIdOrTag || '';

            this.document = o.document || {
                type: '',
                content: ''
            };

            this.lastDocumentType = o.lastDocumentType || '';
            this.documentFetchError = o.documentFetchError || '';
        }

        GetDebuggerProperties() {
            if (!this.isPlayerReady) {
                return [];
            }
            return [
                {
                    title: 'GS - Base',
                    properties: [
                        {
                            name: 'Language',
                            value: this.gs.language
                        },
                        {
                            name: 'Avatar Generator',
                            value: this.gs.avatarGenerator
                        },
                        {
                            name: 'Platform',
                            value: this.gs.platform.type
                        }
                    ]
                },
                {
                    title: 'GS - Ads',
                    properties: [
                        {
                            name: 'Last Ad Success',
                            value: this.isLastAdSuccess
                        },
                        {
                            name: 'Adblock Enabled',
                            value: this.gs.ads.isAdblockEnabled
                        }
                    ]
                },
                {
                    title: 'GS - Leaderboards',
                    properties: [
                        {
                            name: 'Player Position',
                            value: this.leaderboardPlayerPosition
                        }
                    ]
                },
                {
                    title: 'GS - Player',
                    properties: [
                        {
                            name: 'ID',
                            value: this.gs.player.id
                        },
                        {
                            name: 'Logged In By Platform',
                            value: this.gs.player.isLoggedInByPlatform
                        },
                        {
                            name: 'Is Stub',
                            value: this.gs.player.isStub
                        },
                        ...this.gs.player.fields.map((f) => ({
                            name: this.gs.player.getFieldName(f.key),
                            value: this.gs.player.get(f.key),
                            onedit: (v) => this.CallAction(this.actions.PlayerSet, f.key, v)
                        }))
                    ]
                },
                {
                    title: 'GS - Achievements Loop',
                    properties: [
                        {
                            name: 'Current Achievement Index',
                            value: this.currentAchievementIndex
                        },
                        {
                            name: 'Current Achievement ID',
                            value: this.currentAchievementId
                        },
                        {
                            name: 'Current Achievement Tag',
                            value: this.currentAchievementTag
                        },
                        {
                            name: 'Current Achievement Name',
                            value: this.currentAchievementName
                        },
                        {
                            name: 'Current Achievement Description',
                            value: this.currentAchievementDescription
                        },
                        {
                            name: 'Current Achievement Icon',
                            value: this.currentAchievementIcon
                        },
                        {
                            name: 'Current Achievement Icon Small',
                            value: this.currentAchievementIconSmall
                        },
                        {
                            name: 'Current Achievement Icon',
                            value: this.currentAchievementIcon
                        },
                        {
                            name: 'Current Achievement Rare',
                            value: this.currentAchievementRare
                        },
                        {
                            name: 'Current Achievement Unlocked',
                            value: this.currentAchievementUnlocked
                        }
                    ]
                },
                {
                    title: 'GS - Achievements Groups Loop',
                    properties: [
                        {
                            name: 'Current Achievements Group Index',
                            value: this.currentAchievementsGroupIndex
                        },
                        {
                            name: 'Current Achievements Group ID',
                            value: this.currentAchievementsGroupId
                        },
                        {
                            name: 'Current Achievements Group Tag',
                            value: this.currentAchievementsGroupTag
                        },
                        {
                            name: 'Current Achievements Group Name',
                            value: this.currentAchievementsGroupName
                        },
                        {
                            name: 'Current Achievements Group Description',
                            value: this.currentAchievementsGroupDescription
                        }
                    ]
                },
                {
                    title: 'GS - Player Achievements Loop',
                    properties: [
                        {
                            name: 'Current Player Achievement Index',
                            value: this.currentPlayerAchievementIndex
                        },
                        {
                            name: 'Current Player Achievement ID',
                            value: this.currentPlayerAchievementId
                        },
                        {
                            name: 'Current Player Achievement Unlock Date',
                            value: this.currentPlayerAchievementUnlockDate
                        }
                    ]
                },
                {
                    title: 'GS - Unlocked Achievement',
                    properties: [
                        {
                            name: 'Is unlock successful',
                            value: this.isUnlockAchievementSuccess
                        },
                        {
                            name: 'Unlock error',
                            value: this.unlockAchievementError
                        },
                        {
                            name: 'Unlocked Achievement ID',
                            value: this.unlockedAchievementId
                        },
                        {
                            name: 'Unlocked Achievement Tag',
                            value: this.unlockedAchievementTag
                        },
                        {
                            name: 'Unlocked Achievement Name',
                            value: this.unlockedAchievementName
                        },
                        {
                            name: 'Unlocked Achievement Description',
                            value: this.unlockedAchievementDescription
                        },
                        {
                            name: 'Unlocked Achievement Icon',
                            value: this.unlockedAchievementIcon
                        },
                        {
                            name: 'Unlocked Achievement Icon Small',
                            value: this.unlockedAchievementIconSmall
                        },
                        {
                            name: 'Unlocked Achievement Icon',
                            value: this.unlockedAchievementIcon
                        },
                        {
                            name: 'Unlocked Achievement Rare',
                            value: this.unlockedAchievementRare
                        }
                    ]
                },
                {
                    title: 'GS - Products Loop',
                    properties: [
                        {
                            name: 'Current Product Index',
                            value: this.currentProductIndex
                        },
                        {
                            name: 'Current Product ID',
                            value: this.currentProductId
                        },
                        {
                            name: 'Current Product Tag',
                            value: this.currentProductTag
                        },
                        {
                            name: 'Current Product Name',
                            value: this.currentProductName
                        },
                        {
                            name: 'Current Product Description',
                            value: this.currentProductDescription
                        },
                        {
                            name: 'Current Product Icon',
                            value: this.currentProductIcon
                        },
                        {
                            name: 'Current Product Icon Small',
                            value: this.currentProductIconSmall
                        },
                        {
                            name: 'Current Product Icon',
                            value: this.currentProductIcon
                        },
                        {
                            name: 'Current Product Price',
                            value: this.currentProductPrice
                        },
                        {
                            name: 'Current Product Currency',
                            value: this.currentProductCurrency
                        },
                        {
                            name: 'Current Product CurrencySymbol',
                            value: this.currentProductCurrencySymbol
                        },
                        {
                            name: 'Current Product Purchases',
                            value: this.currentProductPurchases
                        }
                    ]
                },
                {
                    title: 'GS - Purchased Product',
                    properties: [
                        {
                            name: 'Is purchase successful',
                            value: this.isPurchaseProductSuccess
                        },
                        {
                            name: 'Purchase error',
                            value: this.purchaseProductError
                        },
                        {
                            name: 'Purchased Product ID',
                            value: this.purchasedProductId
                        },
                        {
                            name: 'Purchased Product Tag',
                            value: this.purchasedProductTag
                        }
                    ]
                },
                {
                    title: 'GS - Consumed Product',
                    properties: [
                        {
                            name: 'Is consume successful',
                            value: this.isConsumeProductSuccess
                        },
                        {
                            name: 'Consume error',
                            value: this.consumeProductError
                        },
                        {
                            name: 'Consumed Product ID',
                            value: this.consumedProductId
                        },
                        {
                            name: 'Consumed Product Tag',
                            value: this.consumedProductTag
                        }
                    ]
                },
                {
                    title: 'GS - Last Games Collection',
                    properties: [
                        {
                            name: 'Collection ID',
                            value: this.gamesCollection.id
                        },
                        {
                            name: 'Collection Tag',
                            value: this.gamesCollection.tag
                        },
                        {
                            name: 'Collection Name',
                            value: this.gamesCollection.name
                        },
                        {
                            name: 'Collection Description',
                            value: this.gamesCollection.description
                        },
                        {
                            name: 'Fetch Error',
                            value: this.gamesCollectionFetchError
                        }
                    ]
                },
                {
                    title: 'GS - Games in Collection',
                    properties: [
                        {
                            name: 'Current Game Index',
                            value: this.currentGameIndex
                        },
                        {
                            name: 'Current Game ID',
                            value: this.currentGameId
                        },
                        {
                            name: 'Current Game Name',
                            value: this.currentGameName
                        },
                        {
                            name: 'Current Game Description',
                            value: this.currentGameDescription
                        },
                        {
                            name: 'Current Game Icon',
                            value: this.currentGameIcon
                        },
                        {
                            name: 'Current Game Url',
                            value: this.currentGameUrl
                        }
                    ]
                },
                {
                    title: 'GS - Documents',
                    properties: [
                        {
                            name: 'Document Type',
                            value: this.document.type
                        },
                        {
                            name: 'Document Content',
                            value: this.document.content
                        },
                        {
                            name: 'Fetch Error',
                            value: this.documentFetchError
                        }
                    ]
                }
            ];
        }
    };
}

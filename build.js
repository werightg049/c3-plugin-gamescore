const zipdir = require('zip-dir');
const fs = require('fs');
const addon = require('./addon/addon.json');
const packageJson = require('./package.json');

const versions = packageJson.version.split('.').map(Number);
// minor
versions[2] += 1;
const version = versions.join('.');

addon.version = version;
packageJson.version = version;

fs.writeFileSync('./package.json', JSON.stringify(packageJson, null, 4));
fs.writeFileSync('./addon/addon.json', JSON.stringify(addon, null, 4));

const pluginPath = './addon/plugin.js';
const plugin = fs.readFileSync(pluginPath).toString();
const pluginWithVersion = plugin.replace(/const PLUGIN_VERSION = '(.+)';/, `const PLUGIN_VERSION = '${version}';`);
fs.writeFileSync(pluginPath, pluginWithVersion);

const instancePath = './addon/c3runtime/instance.js';
const instance = fs.readFileSync(instancePath).toString();
const instanceWithProdUrl = instance.replace(
    "const SERVER_HOST = 'http://localhost:3000';",
    "const SERVER_HOST = 'https://gs.eponesh.com';"
);
fs.writeFileSync(instancePath, instanceWithProdUrl);

const path = `build/game-score-${version}.c3addon`;
const hrstart = process.hrtime();

zipdir('addon', { saveTo: path }, () => {
    fs.writeFileSync(instancePath, instance);

    const [, timeEnd] = process.hrtime(hrstart);
    console.log('Successful build:', path);
    console.info('Execution time: %ds', (timeEnd / 1000000000).toFixed(2));
});
